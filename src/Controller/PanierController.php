<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Basket;
use App\Entity\User;
use App\Entity\Product;

class PanierController extends AbstractController
{
    #[Route('/basket', name: 'app_panier')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $user = $this->getUser();
        $basket = $doctrine->getRepository(Basket::class)->findBy(['user_id' => $user]);
        $produit = $doctrine->getRepository(Product::class)->findAll();
        // dd($basket);
        return $this->render('panier/index.html.twig', [
            'controller_name' => 'Panier',
            'baskets' => $basket,
        ]);
    }
}
