<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Product;
use App\Entity\Tag;
use Symfony\Component\HttpFoundation\Request;


class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(ManagerRegistry $doctrine, Request $request): Response
    {
        $tag = $doctrine->getRepository(Tag::class)->findAll();
        $selectedTag = $doctrine->getRepository(Tag::class);
        $fundSelectedTag = [];
        $product = [];
        if ($request->isMethod('POST')) { 
            
            if($request->get('select-bar-tag') == '' && $request->get('search-bar-by-name') != ''){
            $product = $doctrine->getRepository(Product::class)->findBy(['title' => $request->get('search-bar-by-name')]);
            
            }
            else if($request->get('select-bar-tag') != '' && $request->get('search-bar-by-name') == ''){
                $fundSelectedTag = $selectedTag->findOneBy(['id' => $request->get('select-bar-tag')]);
            }
            // else if($request->get('select-bar-tag') != '' && $request->get('search-bar-by-name') != ''){
            //     $fundSelectedTag = $selectedTag->findOneBy(['id' => $request->get('select-bar-tag')]);
            //     $product = $fundSelectedTag->->findBy(['title' => $request->get('search-bar-by-name')]);
            // }

            return $this->render('index/index.html.twig', [
                'products' => $product,
                'allTags' => $tag,
                'productByTags' => $fundSelectedTag,

           ]);
        }
        return $this->render('index/index.html.twig', [
            'products' => [],
            'allTags' => $tag,
            'productByTags' => [],
        ]);
    }
}