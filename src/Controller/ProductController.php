<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Product;
use App\Entity\Basket;
use App\Entity\User;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Avis;

class ProductController extends AbstractController
{
    #[Route('/product', name: 'app_product')]
    public function index(ManagerRegistry $doctrine, PaginatorInterface $paginator, Request $request): Response
    {
        $donnees = $doctrine->getRepository(Product::class)->findAll();
        $product = $paginator->paginate(
            $donnees, 
            $request->query->getInt('page', 1), 
            10
        );
        return $this->render('product/index.html.twig', [
            'products' => $product
        ]);
    }
    #[Route('/view_product/{id}', name: 'app_view_product')]
    public function viewProduct(ManagerRegistry $doctrine, int $id, Request $request): Response
    {
        $product = $doctrine->getRepository(Product::class)->findOneBy(['id' => $id]);
        if($request->isMethod('POST')){
            $user = $this->getUser();
            $entityManager = $doctrine->getManager();
            $basket = New Basket();
            $basket->setUserId($user);
            $basket->setProductId($product);
            $product->addBasket($basket);
            $user->addBasket($basket);
            $entityManager->persist($basket);
            $entityManager->flush();
        }
        if($request->get('input-titre')){
            $user = $this->getUser();
            $avis = New Avis();
            $avis->setUserId($user);
            $avis->setProductId($product);
            $avis->setTitle($request->get('input-titre'));
            $avis->setContent($request->get('input-content'));
            $entityManager->persist($avis);
            $entityManager->flush();

        }
        return $this->render('product/view_product.html.twig', [
            'product' => $product
        ]);
    }
}
